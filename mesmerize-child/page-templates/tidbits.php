<?php
/*
Template Name: Tidbits
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
					<div id='page-content' class="page-content">
						<div class="header">
							<?php the_title( '<h1 class="hero-title">', '</h1>' ); ?>
						</div>
                    <?php
                    //while (have_posts()) : the_post();
                        //the_content();
                    //endwhile;
                    ?>
					<?php $catquery = new WP_Query( 'cat=3&posts_per_page=-1' ); ?>
					<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
						<div class="full-tidbit">
							<h3 id="<?php the_title(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
							<?php the_content(); ?>
							<p>&nbsp;</p>
							<hr />
							<p>&nbsp;</p>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
                </div>
				</div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
