<?php
/*
Template Name: Obituaries Archive
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
					<div id='page-content' class="page-content">
						<div class="header">
							<?php the_title( '<h1 class="hero-title">', '</h1>' ); ?>
						</div>
                    <?php
                    //while (have_posts()) : the_post();
                        //the_content();
                    //endwhile;
                    ?>
					<div class="obit-archive-button-search">			
						<div class="obit-archive-button">
							<a href="https://dubuquetoday.com/obituaries/" class="obit_button" style="color: #ffffff; text-decoration:none;">See Recent</a>
						</div>

						<div class="obit-search">
							<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<input type="hidden" name="cat" id="cat" value="2" />
								<input type="text" size="16" name="s" placeholder="Search"  />
								<input type="submit" value="Search Obits" />
							</form>
						</div>
					<br style="clear: both;" />
					</div>

					<h3>We post all obituaries for FREE, just have your funeral home send it to us.</h3>

					<?php //$catquery = new WP_Query( 'cat=2&posts_per_page=-1' ); ?>
					<?php //while($catquery->have_posts()) : $catquery->the_post(); ?>
					<?php
							 $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
							 $args = array(
							  'cat' => 2,
							  'orderby' => 'ID',
							  'order' => 'DESC',
							  'posts_per_page' => 15,
							  'paged' => $paged,
							  'date_query' => array(
												 array(
									'before'     => 'midnight 14 days ago',
									'inclusive' => true,
								   ),
								 )
							  );
							 $wp_query = new WP_Query($args);
							 //if($catquery->have_posts()) { 
							 //while ($catquery->have_posts()){$catquery->the_post();
							 while($wp_query->have_posts()) : $wp_query->the_post();
						?>
					<div class="full-obit">
						<h3 id="<?php the_title(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						<?php the_content(); ?>
						<p>&nbsp;</p>
						<hr />
						<p>&nbsp;</p>
					</div>
				<?php endwhile; ?>
					<nav class="pagination">
						<h2>More Results</h2>
						<?php pagination_bar(); ?>
					</nav>
				<?php wp_reset_query(); ?>
                </div>
				</div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
