<?php
/*
Template Name: Homepage with Tidbits
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
					<div class="scroll">
						<?php
						echo do_shortcode( '[hsas-shortcode group="" speed="20" direction="left" gap="100"]' );
						?>
					</div>
					<?php 
						$today = new DateTime();
						$today->setTime(0,0);
						$args = array(
						   'meta_query' => array(
							   array(
								   'key' => 'cover',
								   'value' => $today->format('Y-m-d 00:00'),
								   'compare' => '=',
							   )
						   )
						);
						$tidquery = new WP_Query($args); 
						
						while($tidquery->have_posts()) : $tidquery->the_post();
						?>
						<div class="full-tidbit">
							<h2 id="<?php the_title(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
							<?php the_content(); ?>
						</div>
						<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
                    <?php
                    while (have_posts()) : the_post();
                        the_content();
                    endwhile;
                    ?>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
