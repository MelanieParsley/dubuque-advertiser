<?php
/*
Template Name: Obituaries
*/

mesmerize_get_header();
?>
    <div class="page-content">
        <div class="gridContainer">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
					<div id='page-content' class="page-content">
						<div class="header">
							<?php the_title( '<h1 class="hero-title">', '</h1>' ); ?>
						</div>
                    <?php
                    //while (have_posts()) : the_post();
                        //the_content();
                    //endwhile;
                    ?>
					<div class="obit-archive-button-search">			
						<div class="obit-archive-button"><a href="https://dubuquetoday.com/obituaries-archive/" class="obit_button" style="color: #ffffff; text-decoration:none;">Click here for obituaries<br>older than 14 days</a></div>

						<div class="obit-search">
						<form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<input type="hidden" name="cat" id="cat" value="2" />
							<input type="text" size="16" name="s" placeholder="Search"  />
							<input type="submit" value="Search Obits" />
						</form>
						</div>
					<br style="clear: both;" />
					</div>

					<h3>We post all obituaries for FREE, just have your funeral home send it to us.</h3>
					
					<p><a href="https://flowersbysteves.com" target="_blank">Order flowers and keepsakes locally from The Flower Shoppe at Steve’s Ace Home and Garden</a></p>

					<h2>Current Obituaries</h2>
				
					<div class="obit-list">
						<?php //$catquery = new WP_Query( 'cat=2&orderby=name&order=asc&posts_per_page=-1' ); ?>
						<?php //while($catquery->have_posts()) : $catquery->the_post(); ?> 
						<?php
							 $args = array(
							  'cat' => 2,
							  'orderby'=>'name',
							  'order'=>'ASC',
							  'posts_per_page' => -1,
							  'date_query' => array(
												 array(
									'after'     => 'midnight 14 days ago',
									'inclusive' => true,
								   ),
								 )
							  );
							 $catquery = new WP_Query($args);
							 //if($catquery->have_posts()) { 
							 //while ($catquery->have_posts()){$catquery->the_post();
							 while($catquery->have_posts()) : $catquery->the_post();
						?>
							<div class="obit-list-item"><a href="#<?php the_title() ?>" rel="bookmark"><?php the_title(); ?></a></div>
						<?php endwhile;
							wp_reset_postdata();
						?>
						<br style="clear: both;" />
					</div>

					<?php //$catquery = new WP_Query( 'cat=2&posts_per_page=-1' ); ?>
					<?php //while($catquery->have_posts()) : $catquery->the_post(); ?>
					<?php
							 $args = array(
							  'cat' => 2,
							  'orderby' => 'ID',
							  'order' => 'DESC',
							  'posts_per_page' => -1,
							  'date_query' => array(
												 array(
									'after'     => 'midnight 14 days ago',
									'inclusive' => true,
								   ),
								 )
							  );
							 $catquery = new WP_Query($args);
							 //if($catquery->have_posts()) { 
							 //while ($catquery->have_posts()){$catquery->the_post();
							 while($catquery->have_posts()) : $catquery->the_post();
						?>
					<div class="full-obit">
						<h3 id="<?php the_title(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						<?php the_content(); ?>
						<div style="display: table;">
							<div style="display: table-row;">
								<div style="display: table-cell; vertical-align: middle;"><a href="https://flowersbysteves.com/" target="_blank">Order Flowers & Keepsakes from The Flower Shoppe at Steve's Ace Home and Garden</a></div>
								<div style="display: table=cell; vertical-align: middle;"><a href="https://flowersbysteves.com/" target="_blank"><img src="https://dubuquetoday.com/wp-content/uploads/2019/11/The-Flower-Shoppe.jpg" width="200" /></a></div>
							</div>
						</div>
						<hr />
						<p>&nbsp;</p>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
                </div>
				</div>

                <div class="col-xs-12 col-sm-4 col-md-3 page-sidebar-column">
                    <?php mesmerize_get_sidebar('pages'); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
