<?php

function prefix_category_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'prefix_category_title' );


function mrp_latest_issue() {
	$myarray = glob("*.*");
	$dirname = "images/issues/";
	$images = glob($dirname."*.*");
	usort($images, create_function('$b,$a', 'return filemtime($a) - filemtime($b);'));
	$i=0;
	foreach($images as $image) {
		ob_start();
		$path_parts = pathinfo($image);
		return '<p><a href="https://www.dubuquetoday.com/issueembed.cfm"><img src="'.home_url().'/images/issues/'.$path_parts['filename'].'.jpg" width="125" alt="This Week\'s Issue" /></a></p>'; 
		$i++;
		if($i==1) break;
		
		return ob_get_clean();
	}
}
add_shortcode('latest_issue', 'mrp_latest_issue');

function mrp_sidebar_ads() {
	if ( is_page( 84 ) ) {
		//Obituaries
		return '<div id="sidebar-ads">'.adrotate_group(7).'</div>';
	} elseif ( is_page( 14 ) ) {
		//Home
		return adrotate_group(8);
	} elseif ( is_page( 157 ) ) {
		//About
		return adrotate_group(17);
	} elseif ( is_page( 168 ) ) {
		//Circulation
		return adrotate_group(18);
	} elseif ( is_page( 179 ) ) {
		//Contact Us
		return adrotate_group(19);
	} elseif ( is_page( 175 ) ) {
		//Display Advertising
		return adrotate_group(20);
	} elseif ( is_page( 154 ) ) {
		//Tidbits
		return adrotate_group(21);
	} elseif ( is_page( 25 ) ) {
		//Auctions
		return adrotate_group(22);
	} elseif ( is_page( 51 ) ) {
		//Commercial Printing
		return adrotate_group(23);
	} elseif ( is_page( 48 ) ) {
		//Community Links
		return adrotate_group(24);
	} elseif ( is_page( 227 ) ) {
		//Obituaries Archive
		return adrotate_group(25);
	} elseif ( is_page( 43 ) ) {
		//Weddings
		return adrotate_group(26);
	} elseif ( is_page( 190 ) ) {
		//Weekly Ads
		return adrotate_group(27);
	} elseif ( is_page( 38 ) ) {
		//Community Calendar
		return adrotate_group(29);
	} else {
		//code to be executed if all conditions are false;
	}
}
add_shortcode('sidebar_ads', 'mrp_sidebar_ads');

function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}
?>
