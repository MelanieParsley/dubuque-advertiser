<div class="clearfix weekly-ads">
	<h2 class="title">Weekly Ads</h2>
	
	<p><a href="https://dubuquetoday.com/weekly-ads/">Click here to view ads as a list.</a></p>
	
	<br style="clear: both;" />
	
	<div id="photos">
		<?php 
		$dirname = "ftp/weekly_ads/";
		$images = glob($dirname."*.*");

		foreach($images as $image) {
			$path_parts = pathinfo($image);
			echo '<a href="'.home_url().'/ftp/weekly_ads/'.$path_parts['filename'].'.pdf" target="_blank"><img src="'.home_url().'/ftp/weekly_ads/thumbs/'.$path_parts['filename'].'_page_1.jpg" /></a>';
		}
		?>
	</div>
</div>

<div <?php echo mesmerize_footer_container('footer-content-lists footer-border-accent') ?>>
    <div <?php echo mesmerize_footer_background('footer-content') ?>>
        <div class="gridContainer">
            <div class="row">
                <div class="col-sm-12 flexbox">
                    <div class="row widgets-row">
                        <div class="col-sm-4">
                            <?php
                               mesmerize_print_widget('first_box_widgets');
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                               mesmerize_print_widget('second_box_widgets');
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                                mesmerize_print_widget('third_box_widgets');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div <?php echo mesmerize_footer_container('footer-simple') ?>>
    <div <?php echo mesmerize_footer_background('footer-content center-xs') ?>>
        <div class="gridContainer">
	        <div class="row middle-xs footer-content-row">
	            <div class="footer-content-col col-xs-12">
	                <?php //echo mesmerize_get_footer_copyright(); ?>
					<p>&copy; <?php echo date('Y'); ?>  <a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a></p>
	            </div>
	        </div>
	    </div>
    </div>
</div>
