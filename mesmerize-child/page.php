<?php mesmerize_get_header(); ?>

    <div class="page-content">
			<?php if ( is_front_page() ) { ?>
				<div class="scroll">
				<?php
				echo do_shortcode( '[hsas-shortcode group="" speed="20" direction="left" gap="100"]' );
				?>
				</div>
			<?php
            } ?>
        <div class="<?php mesmerize_page_content_wrapper_class(); ?>">
            <?php
            while (have_posts()) : the_post();
                get_template_part('template-parts/content', 'page');
            endwhile;
            ?>
        </div>
    </div>

<?php get_footer(); ?>
