<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div  id="page-top" class="header-top">
	<?php 
	if ( is_page( 25 ) ) {
		//Auctions
		echo adrotate_group(1);
	} elseif ( is_page( 14 ) ) {
		//Home
		echo adrotate_group(2);
	} elseif ( is_page( 84 ) ) {
		//Obituaries
		echo adrotate_group(3);
	} elseif ( is_page( 154 ) ) {
		//Tidbits
		echo adrotate_group(4);
	} elseif ( is_page( 43 ) ) {
		//Weddings
		echo adrotate_group(5);
	} elseif ( is_page( 157 ) ) {
		//About
		echo adrotate_group(9);
	} elseif ( is_page( 168 ) ) {
		//Circulation
		echo adrotate_group(10);
	} elseif ( is_page( 179 ) ) {
		//Contact Us
		echo adrotate_group(11);
	} elseif ( is_page( 175 ) ) {
		//Display Advertising
		echo adrotate_group(12);
	} elseif ( is_page( 51 ) ) {
		//Commercial Printing
		echo adrotate_group(13);
	} elseif ( is_page( 48 ) ) {
		//Community Links
		echo adrotate_group(14);
	} elseif ( is_page( 227 ) ) {
		//Obituaries Archive
		echo adrotate_group(15);
	} elseif ( is_page( 190 ) ) {
		//Weekly Ads
		echo adrotate_group(16);
	} elseif ( is_page( 38 ) ) {
		//Community Calendar
		echo adrotate_group(28);
	} else {
		//code to be executed if all conditions are false;
	}
	?>
	<?php mesmerize_print_header_top_bar(); ?>
	<?php mesmerize_get_navigation(); ?>
</div>

<div id="page" class="site">
